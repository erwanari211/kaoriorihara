<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php
// delete absensi data
$id = $_GET['id'];
$sql = "DELETE FROM `absensi` WHERE `id`=$id";
$result = mysqli_query($conn, $sql);

// delete absensi detail data
$sql = "DELETE FROM `absensi_detail` WHERE `absensi_id`=$id";
$result = mysqli_query($conn, $sql);
 
// redirect to absensi list
header("Location:index.php");
?>