<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'absensi';

// If form submitted, update data.
if(isset($_POST['submit'])) {
    // get input data
    $input = $_POST['input'];

    // update absensi detail data
    foreach ($input as $absensiDetailId => $item) {
        $sql = "UPDATE `absensi_detail` SET `keterangan`='{$item['keterangan']}' WHERE `id`=$absensiDetailId";
        $result = mysqli_query($conn, $sql);
    }
    $successMessage = "Data sudah diperbarui.";
}

// get absensi data
$id = $_GET['id'];
$sql = "";
$sql .= "SELECT `absensi`.*, `kelas`.`kelas_no`, `kelas`.`kelas`, `kelas`.`subkelas`";
$sql .= "FROM `absensi`";
$sql .= "LEFT JOIN `kelas` ON `kelas`.`id` = `absensi`.`kelas_id`";
$sql .= "WHERE `absensi`.`id` = $id";

$result = mysqli_query($conn, $sql);
$absensiData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $absensiData = $row;
}

// get absensi detail and siswa
$sql = "";
$sql .= "SELECT `absensi_detail`.*, `siswa`.`nama`";
$sql .= "FROM `absensi_detail`";
$sql .= "LEFT JOIN `siswa` ON `siswa`.`id` = `absensi_detail`.`siswa_id`";
$sql .= "WHERE `absensi_id` = $id ";
$sql .= "ORDER BY `siswa`.`nama`";
$result = mysqli_query($conn, $sql);
$siswaData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $siswaData[] = $row;
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Detail Absensi | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Detail Absensi</h3>
                    </div>
                    <div class="panel-body">
                        <h4>
                            Absensi 
                            Kelas <?= $absensiData['kelas'] ?> <?= $absensiData['subkelas'] ?>
                            tanggal <?= date('d M Y', strtotime($absensiData['tanggal'])) ?> 
                        </h4>
                        <hr>
                        <?php include('../layouts/messages.php'); ?>
                        <div class="well">
                            <p><strong>Keterangan</strong></p>
                            H = Hadir <br>
                            S = Sakit <br>
                            I = Ijin <br>
                            A = Alpa <br>
                        </div>
                        <form action="detail.php?id=<?= $id ?>" method="POST" role="form">                        
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; ?>
                                        <?php foreach ($siswaData as $item): ?>
                                            <tr>
                                                <td><?= $no ?></td>
                                                <td><?= $item['nama'] ?></td>
                                                <td>
                                                    <div class="form-group">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="input[<?= $item['id'] ?>][keterangan]" 
                                                                value="sakit" 
                                                                <?= ($item['keterangan']  == 'sakit') ? 'checked="checked"' : '' ?>> S
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="input[<?= $item['id'] ?>][keterangan]" 
                                                                value="ijin"
                                                                <?= ($item['keterangan']  == 'ijin') ? 'checked="checked"' : '' ?>> I
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="input[<?= $item['id'] ?>][keterangan]" 
                                                                value="alpa"
                                                                <?= ($item['keterangan']  == 'alpa') ? 'checked="checked"' : '' ?>> A
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="input[<?= $item['id'] ?>][keterangan]" 
                                                                value="hadir"
                                                                <?= ($item['keterangan']  == 'hadir') ? 'checked="checked"' : '' ?>> H
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>      
                                            <?php $no++ ?>                                      
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>


                            <input class="btn btn-primary" type="submit" name="submit" value="Edit">
                            <a class="btn btn-default" href="index.php">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>

        <script src="<?= baseUrl() ?>/assets/plugins/moment/min/moment.min.js"></script>
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script>
            $(document).ready(function() {
                $('.datetimepicker').datetimepicker({
                    format: 'YYYY-MM-DD' 
                });
            });
        </script>
    </body>
</html>