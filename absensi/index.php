<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'absensi';

// get absensi data
$sql = "";
$sql .= "SELECT `absensi`.*, `kelas`.`kelas_no`, `kelas`.`kelas`, `kelas`.`subkelas`";
$sql .= "FROM `absensi`";
$sql .= "LEFT JOIN `kelas` ON `kelas`.`id` = `absensi`.`kelas_id`";
$sql .= "ORDER BY `absensi`.`tanggal` DESC, `kelas`.`kelas_no` ASC, `kelas`.`subkelas` ASC";

$result = mysqli_query($conn, $sql);
$absensiData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $absensiData[] = $row;
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>List Absensi | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">List Absensi</h3>
                </div>
                <div class="panel-body">
                    <a class="btn btn-primary" href="add.php">Tambah Absensi</a>
                    <br> <br>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Kelas</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($absensiData as $key => $item): ?>
                                    <tr>
                                        <td><?= $item['tanggal'] ?></td>
                                        <td><?= $item['kelas'] ?> <?= $item['subkelas'] ?></td>
                                        <td>
                                            <!-- <a class="btn btn-success btn-xs" href="edit.php?id=<?= $item['id'] ?>">Edit</a>  -->
                                            <a class="btn btn-success btn-xs" href="detail.php?id=<?= $item['id'] ?>">Detail</a> 
                                            <form action="delete.php?id=<?= $item['id'] ?>" method="POST" style="display:inline;">
                                                <input class="btn btn-danger btn-xs" type="submit" name="submit" value="Delete">
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>