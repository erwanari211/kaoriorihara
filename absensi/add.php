<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'absensi';

// get kelas
$sql = "SELECT * FROM `kelas` ORDER BY `kelas_no` ASC, `subkelas` ASC";
$result = mysqli_query($conn, $sql);
$kelasData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $kelasData[] = $row;
}

// If form submitted, insert data.
if(isset($_POST['submit'])) {
    // get input data
    $tanggal = $_POST['tanggal'];
    $kelas_id = $_POST['kelas_id'];

    // check if absensi exist
    $query = "SELECT count(id) as `total` FROM `absensi` WHERE `tanggal` = '$tanggal' AND kelas_id = $kelas_id";     
    $result = mysqli_query($conn, $query);
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $check = $row;
    }
    
    // if exist, display error message
    if ($check['total'] > 0) {
        $errorMessage = "Data absensi sudah ada";    
    } else {
        // if not exist, insert data
        $query = "INSERT INTO `absensi` (tanggal,kelas_id) VALUES ('$tanggal','$kelas_id')";
        $result = mysqli_query($conn, $query);
        $lastId = mysqli_insert_id($conn);
        
        // get absensi
        $absensiId = $lastId;
        $sql = "SELECT * FROM `absensi` WHERE `id` = $lastId";
        $result = mysqli_query($conn, $sql);
        $absensiData = array();
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $absensiData = $row;
        }

        // get kelas id from absensi
        $kelasId = $absensiData['kelas_id'];
        
        // get siswa by kelas id
        $sql = "SELECT * FROM `siswa` WHERE `kelas_id` = $kelasId";
        $result = mysqli_query($conn, $sql);
        $siswaData = array();
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $siswaData[] = $row;
        }

        // check if absensi detail exist
        $query = "SELECT count(id) as `total` FROM `absensi_detail` WHERE `absensi_id` = '$absensiId'";     
        $result = mysqli_query($conn, $query);
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $checkDetail = $row;
        }

        // if absensi detail not exist
        if (!$checkDetail['total'] > 0) {
            // insert absensi detail
            foreach ($siswaData as $siswa) {
                $query = "INSERT INTO `absensi_detail` (absensi_id,siswa_id) VALUES ($absensiId, {$siswa['id']})";
                $result = mysqli_query($conn, $query);
            }
        }

        $successMessage = "Data sudah tersimpan. <a href='index.php'>Lihat list absensi</a>";
    }
            
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tambah Absensi | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tambah Absensi</h3>
                    </div>
                    <div class="panel-body">
                        <?php include('../layouts/messages.php'); ?>
                        <form action="add.php" method="POST" role="form">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="text" class="form-control datetimepicker" name="tanggal" required="required">
                            </div>

                            <div class="form-group">
                                <label>Kelas</label>
                                <select name="kelas_id" class="form-control" required="required">
                                    <option value="">Pilih Kelas</option>
                                    <?php foreach ($kelasData as $key => $item): ?>
                                        <option value="<?= $item['id'] ?>">
                                            <?= $item['kelas'] ?> <?= $item['subkelas'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>                         

                            <input class="btn btn-primary" type="submit" name="submit" value="Tambah">
                            <a class="btn btn-default" href="index.php">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>

        <script src="<?= baseUrl() ?>/assets/plugins/moment/min/moment.min.js"></script>
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script>
            $(document).ready(function() {
                $('.datetimepicker').datetimepicker({
                    format: 'YYYY-MM-DD' 
                });
            });
        </script>
        
    </body>
</html>