<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'absensi';

// get kelas
$sql = "SELECT * FROM `kelas` ORDER BY `kelas_no` ASC, `subkelas` ASC";
$result = mysqli_query($conn, $sql);
$kelasData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $kelasData[] = $row;
}

// If form submitted, update data.
if(isset($_POST['submit'])) {
    // get input data
    $id = $_POST['id'];
    $tanggal = $_POST['tanggal'];
    $kelas_id = $_POST['kelas_id'];

    // check if absensi exist
    $query = "SELECT count(id) as `total` FROM `absensi` WHERE `tanggal` = '$tanggal' AND `kelas_id` = $kelas_id AND `id` <> $id";    
    $result = mysqli_query($conn, $query);
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $check = $row;
    }

    // if exist, display error message
    if ($check['total'] > 0) {
        $errorMessage = "Data absensi sudah ada";    
    } else {
        // if not exist, update data
        $sql = "UPDATE `absensi` SET `tanggal`='$tanggal', `kelas_id`='$kelas_id' WHERE `id`=$id";
        $result = mysqli_query($conn, $sql);
        $successMessage = "Data sudah diperbarui. <a href='index.php'>Lihat list absensi</a>";
    }
            
}

// get absensi
$id = $_GET['id'];
$sql = "SELECT * FROM `absensi` WHERE `id` = $id";
$result = mysqli_query($conn, $sql);
$absensiData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $absensiData = $row;
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Absensi | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Absensi</h3>
                    </div>
                    <div class="panel-body">
                        <?php include('../layouts/messages.php'); ?>
                        <form action="edit.php?id=<?= $id ?>" method="POST" role="form">                        
                            <div class="form-group hide">
                                <label>ID</label>
                                <input type="hidden" class="form-control" name="id" required="required" value="<?= $absensiData['id'] ?>">
                            </div>

                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="text" class="form-control datetimepicker" name="tanggal" required="required" value="<?= $absensiData['tanggal'] ?>">
                            </div>

                            <div class="form-group">
                                <label>Kelas</label>
                                <select name="kelas_id" class="form-control" required="required">
                                    <option value="">Pilih Kelas</option>
                                    <?php foreach ($kelasData as $key => $item): ?>
                                        <?php if ($item['id'] == $absensiData['kelas_id']): ?>
                                            <option value="<?= $item['id'] ?>" selected>
                                                <?= $item['kelas'] ?> <?= $item['subkelas'] ?>
                                            </option>
                                        <?php else: ?>      
                                            <option value="<?= $item['id'] ?>">
                                                <?= $item['kelas'] ?> <?= $item['subkelas'] ?>
                                            </option>
                                        <?php endif ?> 
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <input class="btn btn-primary" type="submit" name="submit" value="Edit">
                            <a class="btn btn-default" href="index.php">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>

        <script src="<?= baseUrl() ?>/assets/plugins/moment/min/moment.min.js"></script>
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script>
            $(document).ready(function() {
                $('.datetimepicker').datetimepicker({
                    format: 'YYYY-MM-DD' 
                });
            });
        </script>
    </body>
</html>