<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'kelas';

// If form submitted, insert data.
if(isset($_POST['submit'])) {
    // get input data
    $kelas_no = $_POST['kelas_no'];
    $kelas = $_POST['kelas'];
    $subkelas = $_POST['subkelas'];
            
    // insert kelas data
    $sql = "INSERT INTO `kelas` (kelas_no,kelas,subkelas) VALUES ('$kelas_no','$kelas', '$subkelas')";
    $result = mysqli_query($conn, $sql);
    $successMessage = "Data sudah tersimpan. <a href='index.php'>Lihat list kelas</a>";
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tambah Kelas | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tambah Kelas</h3>
                    </div>
                    <div class="panel-body">
                        <?php include('../layouts/messages.php'); ?>
                        <form action="add.php" method="POST" role="form">
                            <div class="form-group">
                                <label>Kelas (Tingkat)</label>
                                <input type="number" class="form-control" name="kelas_no" required="required" placeholder="diisi dengan angka, misal 5">
                            </div>

                            <div class="form-group">
                                <label>Kelas (Romawi)</label>
                                <input type="text" class="form-control" name="kelas" required="required" placeholder="diisi dengan romawi, misal V">
                            </div>

                            <div class="form-group">
                                <label>Subkelas</label>
                                <input type="text" class="form-control" name="subkelas" required="required" placeholder="diisi dengan huruf">
                            </div>
                           

                            <input class="btn btn-primary" type="submit" name="submit" value="Tambah">
                            <a class="btn btn-default" href="index.php">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>