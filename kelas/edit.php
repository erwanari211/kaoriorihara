<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'kelas';

// If form submitted, update data.
if(isset($_POST['submit'])) {
    // get input data
    $id = $_POST['id'];
    $kelas_no = $_POST['kelas_no'];
    $kelas = $_POST['kelas'];
    $subkelas = $_POST['subkelas'];
            
    // update kelas data
    $sql = "UPDATE `kelas` SET `kelas_no`='$kelas_no', `kelas`='$kelas', `subkelas`='$subkelas' WHERE `id`=$id";
    $result = mysqli_query($conn, $sql);
    $successMessage = "Data sudah diperbarui. <a href='index.php'>Lihat list kelas</a>";
}

// get kelas data
$id = $_GET['id'];
$sql = "SELECT * FROM `kelas` WHERE `id` = $id";
$result = mysqli_query($conn, $sql);
$kelasData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $kelasData = $row;
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Siswa | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Siswa</h3>
                    </div>
                    <div class="panel-body">
                        <?php include('../layouts/messages.php'); ?>
                        <form action="edit.php?id=<?= $id ?>" method="POST" role="form">                        
                            <div class="form-group hide">
                                <label>ID</label>
                                <input type="hidden" class="form-control" name="id" required="required" value="<?= $kelasData['id'] ?>">
                            </div>

                            <div class="form-group">
                                <label>Kelas (Tingkat)</label>
                                <input type="number" class="form-control" name="kelas_no" required="required" placeholder="diisi dengan angka, misal 5" value="<?= $kelasData['kelas_no'] ?>">
                            </div>

                            <div class="form-group">
                                <label>Kelas (Romawi)</label>
                                <input type="text" class="form-control" name="kelas" required="required" placeholder="diisi dengan romawi, misal V" value="<?= $kelasData['kelas'] ?>">
                            </div>

                            <div class="form-group">
                                <label>Subkelas</label>
                                <input type="text" class="form-control" name="subkelas" required="required" placeholder="diisi dengan huruf" value="<?= $kelasData['subkelas'] ?>">
                            </div>

                            <input class="btn btn-primary" type="submit" name="submit" value="Edit">
                            <a class="btn btn-default" href="index.php">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>