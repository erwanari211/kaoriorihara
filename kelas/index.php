<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'kelas';

// get kelas data
$sql = "SELECT * FROM `kelas` ORDER BY `kelas_no` ASC, `subkelas` ASC";
$result = mysqli_query($conn, $sql);
$kelasData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $kelasData[] = $row;
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>List Kelas | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">List Kelas</h3>
                </div>
                <div class="panel-body">
                    <a class="btn btn-primary" href="add.php">Tambah Kelas</a>
                    <br> <br>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Kelas (Tingkat)</th>
                                    <th>Kelas (Romawi)</th>
                                    <th>Subkelas</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($kelasData as $key => $item): ?>
                                    <tr>
                                        <td><?= $item['kelas_no'] ?></td>
                                        <td><?= $item['kelas'] ?></td>
                                        <td><?= $item['subkelas'] ?></td>
                                        <td>
                                            <a class="btn btn-success btn-xs" href="edit.php?id=<?= $item['id'] ?>">Edit</a> 
                                            <form action="delete.php?id=<?= $item['id'] ?>" method="POST" style="display:inline;">
                                                <input class="btn btn-danger btn-xs" type="submit" name="submit" value="Delete">
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>