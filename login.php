<?php include_once('system/config.php'); ?>
<?php include_once('system/functions.php'); ?>
<?php 
?>

<?php 
session_start(); 

// if already login redirect to home
if (isset($_SESSION['isLogin']) && $_SESSION['isLogin']) {
    header("location:index.php");
}

// If form submitted, check if login is valid
if(isset($_POST['submit'])) {
    
    // get input data
    $username = $_POST['username'];
    $password = $_POST['password'];
            
    // get user from db
    $sql = "SELECT * FROM `users` WHERE `username`= '$username' AND `password` = '$password' ";
    $result = mysqli_query($conn, $sql);
    $check = mysqli_num_rows($result);

    // if user exist, status login true
    if ($check > 0) {        
        $_SESSION['username'] = $username;
        $_SESSION['isLogin'] = true;
        // redirect to home
        header("location:index.php");
    } else {
        // if user not exist, display message
        $errorMessage = "Invalid Login";
    }

}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        <?php include('layouts/messages.php'); ?>
                        <form action="login.php" method="POST" role="form">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" required="required">
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" required="required">
                            </div>

                            <input class="btn btn-primary" type="submit" name="submit" value="Login">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>