<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'siswa';

// get kelas
$sql = "SELECT * FROM `kelas` ORDER BY `kelas_no` ASC, `subkelas` ASC";
$result = mysqli_query($conn, $sql);
$kelasData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $kelasData[] = $row;
}

// If form submitted, insert data.
if(isset($_POST['submit'])) {
    // get input data
    $nama = $_POST['nama'];
    $kelas_id = $_POST['kelas_id'];
            
    // insert siswa data
    $sql = "INSERT INTO `siswa` (nama,kelas_id) VALUES ('$nama','$kelas_id')";
    $result = mysqli_query($conn, $sql);
    $successMessage = "Data sudah tersimpan. <a href='index.php'>Lihat list siswa</a>";
}
?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tambah Siswa | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tambah Siswa</h3>
                    </div>
                    <div class="panel-body">
                        <?php include('../layouts/messages.php'); ?>
                        <form action="add.php" method="POST" role="form">
                            <div class="form-group">
                                <label>Nama Siswa</label>
                                <input type="text" class="form-control" name="nama" required="required">
                            </div>

                            <div class="form-group">
                                <label>Kelas</label>
                                <select name="kelas_id" class="form-control" required="required">
                                    <option value="">Pilih Kelas</option>
                                    <?php foreach ($kelasData as $key => $item): ?>
                                        <option value="<?= $item['id'] ?>">
                                            <?= $item['kelas'] ?> <?= $item['subkelas'] ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <input class="btn btn-primary" type="submit" name="submit" value="Tambah">
                            <a class="btn btn-default" href="index.php">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>