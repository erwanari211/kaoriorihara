<?php 

function checkLogin()
{
    // if not login, redirect to login page
    if (!isset($_SESSION['isLogin']) OR !$_SESSION['isLogin']) {
        $redirectTo = baseUrl().'/login.php';
        header("location:$redirectTo");
    }
}