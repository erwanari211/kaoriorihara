<?php include_once('../system/config.php'); ?>
<?php include_once('../system/functions.php'); ?>

<?php 
session_start(); 
// if not login, redirect to login page
checkLogin(); 
?>

<?php 
$activeMenu = 'laporan';

// get kelas
$sql = "SELECT * FROM `kelas` ORDER BY `kelas_no` ASC, `subkelas` ASC";
$result = mysqli_query($conn, $sql);
$kelasData = array();
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $kelasData[] = $row;
}

$tanggal = NULL;
$kelas_id = NULL;

// If form submitted, display report
if(isset($_POST['submit'])) {
    // get input data
    $tanggal = $_POST['tanggal'];
    $kelas_id = $_POST['kelas_id'];
    // check if siswa is selected
    $siswa_id = isset($_POST['siswa_id']) ? $_POST['siswa_id'] : NULL;

    // get first and last date of the month
    $firstDay = strtotime($tanggal);
    $lastDay = strtotime("+1 month -1 day", $firstDay);
    $startDate = date('Y-m-d', $firstDay);
    $endDate = date('Y-m-d', $lastDay);

    // SELECT *, absensi_detail.keterangan, (absensi_detail.id) as total
    // FROM absensi_detail
    // LEFT JOIN absensi ON absensi.id = absensi_detail.absensi_id
    // LEFT JOIN kelas ON kelas.id = absensi.kelas_id
    // LEFT JOIN siswa ON siswa.id = absensi_detail.siswa_id
    // WHERE (tanggal BETWEEN '2018-04-01' AND '2018-04-30')
    // GROUP BY keterangan

    // if siswa not selected, fet data siswa
    if ($siswa_id) {
        // get absensi detail data
        $sql = "";
        $sql .= "SELECT `absensi_detail`.`keterangan`, count(`absensi_detail`.`id`) as total ";
        $sql .= "FROM `absensi_detail` ";
        $sql .= "LEFT JOIN `absensi` ON `absensi`.`id` = `absensi_detail`.`absensi_id` ";
        $sql .= "LEFT JOIN `kelas` ON `kelas`.`id` = `absensi`.`kelas_id` ";
        $sql .= "LEFT JOIN `siswa` ON `siswa`.`id` = `absensi_detail`.`siswa_id` ";
        $sql .= "WHERE (`tanggal` BETWEEN '$startDate' AND '$endDate') ";
        $sql .= "AND `absensi`.`kelas_id` = $kelas_id ";
        $sql .= "AND `absensi_detail`.`siswa_id` = $siswa_id ";
        $sql .= "GROUP BY `keterangan`";

        $result = mysqli_query($conn, $sql);
        $resultData = array();
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $resultData[] = $row;
        }

        $laporanData = array();
        foreach ($resultData as $item) {
            $laporanData[$item['keterangan']] = $item['total'];
        }
    } 

    // get siswa data
    $sql = "";
    $sql .= "SELECT * ";
    $sql .= "FROM `siswa` ";
    $sql .= "WHERE `kelas_id` = $kelas_id ";
    $sql .= "ORDER BY `nama` ASC";
    $result = mysqli_query($conn, $sql);
    $siswaData = array();
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $siswaData[] = $row;
    }            
}

?>

<!DOCTYPE html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laporan Per Kelas | <?= APP_NAME ?></title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery -->
        <script src="<?= baseUrl() ?>/assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <?php include('../layouts/navbar.php'); ?>

        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Laporan Per Kelas</h3>
                    </div>
                    <div class="panel-body">
                        <?php include('../layouts/messages.php'); ?>
                        <form action="laporan-siswa.php" method="POST" role="form">
                            <div class="form-group">
                                <label>Bulan</label>
                                <input type="text" class="form-control datetimepicker" name="tanggal" required="required" value="<?= $tanggal ?>">
                            </div>

                            <div class="form-group">
                                <label>Kelas</label>
                                <select name="kelas_id" class="form-control" required="required">
                                    <option value="">Pilih Kelas</option>
                                    <?php foreach ($kelasData as $key => $item): ?>
                                        <?php if ($kelas_id && ($item['id'] == $kelas_id)): ?>
                                            <option value="<?= $item['id'] ?>" selected>
                                                <?= $item['kelas'] ?> <?= $item['subkelas'] ?>
                                            </option>
                                        <?php else: ?>      
                                            <option value="<?= $item['id'] ?>">
                                                <?= $item['kelas'] ?> <?= $item['subkelas'] ?>
                                            </option>
                                        <?php endif ?> 
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <?php if (isset($siswaData)): ?>
                                <div class="form-group">
                                    <label>Siswa</label>
                                    <select name="siswa_id" class="form-control" required="required">
                                        <option value="">Pilih Siswa</option>
                                        <?php foreach ($siswaData as $key => $item): ?>
                                            
                                            <?php if ($siswa_id && ($item['id'] == $siswa_id)): ?>
                                                <option value="<?= $item['id'] ?>" selected>
                                                    <?= $item['nama'] ?>
                                                </option>
                                            <?php else: ?>      
                                                <option value="<?= $item['id'] ?>">
                                                    <?= $item['nama'] ?>
                                                </option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            <?php endif ?>

                           

                            <input class="btn btn-primary" type="submit" name="submit" value="Tampilkan">
                            <a class="btn btn-default" href="index.php">Back</a>
                        </form>

                        

                        <?php if (isset($laporanData)): ?>
                            
                            <hr>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Keterangan</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Hadir</td>
                                            <td><?= isset($laporanData['hadir']) ? $laporanData['hadir'] : 0 ?></td>
                                        </tr>
                                        <tr>
                                            <td>Sakit</td>
                                            <td><?= isset($laporanData['sakit']) ? $laporanData['sakit'] : 0 ?></td>
                                        </tr>
                                        <tr>
                                            <td>Ijin</td>
                                            <td><?= isset($laporanData['ijin']) ? $laporanData['ijin'] : 0 ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alpa</td>
                                            <td><?= isset($laporanData['alpa']) ? $laporanData['alpa'] : 0 ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                
            </div>
        </div>
        
        <!-- Bootstrap JavaScript -->
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js"></script>

        <script src="<?= baseUrl() ?>/assets/plugins/moment/min/moment.min.js"></script>
        <script src="<?= baseUrl() ?>/assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script>
            $(document).ready(function() {
                $('.datetimepicker').datetimepicker({
                    viewMode: 'months',
                    format: 'YYYY-MM' 
                });
            });
        </script>
    </body>
</html>