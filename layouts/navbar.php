<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= baseUrl() ?>">ABSENSI</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= (isset($activeMenu) && $activeMenu == 'kelas') ? 'active' : '' ?>">
                    <a href="<?= baseUrl().'/kelas/index.php' ?>">Kelas</a>
                </li>
                <li class="<?= (isset($activeMenu) && $activeMenu == 'siswa') ? 'active' : '' ?>">
                    <a href="<?= baseUrl().'/siswa/index.php' ?>">Siswa</a>
                </li>
                <li class="<?= (isset($activeMenu) && $activeMenu == 'absensi') ? 'active' : '' ?>">
                    <a href="<?= baseUrl().'/absensi/index.php' ?>">Absensi</a>
                </li>
                <li class="<?= (isset($activeMenu) && $activeMenu == 'laporan') ? 'active' : '' ?>">
                    <a href="<?= baseUrl().'/laporan/index.php' ?>">Laporan</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_SESSION['isLogin']) && $_SESSION['isLogin']): ?>
                    <li>
                        <a href="#"><?= strtoupper($_SESSION['username']) ?></a>
                    </li>
                    <li><a href="<?= baseUrl().'/logout.php' ?>">Logout</a></li>
                <?php else: ?>
                    <li><a href="<?= baseUrl().'/login.php' ?>">Login</a></li>
                <?php endif ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>