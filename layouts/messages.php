<?php if (isset($successMessage)): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?= $successMessage ?>
    </div>
<?php endif ?>

<?php if (isset($errorMessage)): ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?= $errorMessage ?>
    </div>
<?php endif ?>